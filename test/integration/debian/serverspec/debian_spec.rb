require 'serverspec'
set :backend, :exec

describe 'Packages' do
  [
    'apt-transport-https',
    'ca-certificates',
    'curl',
    'software-properties-common',
    'docker-ce',
    'docker-compose'
  ].each do |pkg|
    describe package(pkg) do
      it { should be_installed }
    end
  end
end

describe 'Services' do
  describe service('docker') do
    it { should be_running }
  end
end

conf = '/opt/homework/docker-compose.yml'
describe 'Containers' do
  [
    'front',
    'imagesserver',
    'nginx'
  ].each do |c|
    describe command("docker-compose -f #{conf} ps|grep #{c}|grep -q Up") do
      its(:exit_status) { should eq 0 }
    end
  end
end
