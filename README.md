# ansible-homework ![License][license-img]

1. [Overview](#overview)
1. [Setup](#setup)
1. [Usage](#usage)
1. [Limitations](#limitations)
1. [Development](#development)
1. [Miscellaneous](#miscellaneous)

## Overview

This role is a simple way to install and deploy the homework project.

## Setup

Dependencies:
- Vagrant
- Virtualbox

Development dependencies:
- Ruby
- Test-kitchen (gem)

Copy the whole project into your ansible role repository and call the role in
your playbook.

## Usage

Use default configuration.

```yaml
- hosts: localhost
  roles:
    - ansible-homework
```

## Limitations

So far, this is compatible with Debian and other derivatives.
Tested on Debian 9-5.

## Development

Please read carefully [CONTRIBUTING.md]
(https://git.vpgrp.io/ansible/ansible-rules/raw/master/CONTRIBUTING.md)
before making a merge request.

## Miscellaneous

[license-img]: https://img.shields.io/badge/license-Apache-blue.svg
