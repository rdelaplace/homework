Vagrant.configure(2) do |config|
  config.vm.provision "shell", inline: <<-SHELL
     sudo apt-get update
     sudo apt-get install -y ansible ruby ruby-dev
  SHELL
end
